package teamlab;
public class Shapes {
    public static void main(String[] args) {
        Triangle t = new Triangle(6, 3, 2, 4);
        System.out.println(t);
        System.out.println("Area: " + t.getArea());
        System.out.println("Perimeter: " + t.getPerimeter());

        Square s = new Square(5,5);
        System.out.println(s);

    }
}