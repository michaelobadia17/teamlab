package teamlab;
public class Square { 
    private double width;
    private double length;

    public Square(double width, double length){
        this.width=width;
        this.length=length;
    }

    public double getLength(){
        return this.length;
    }
    public double getWidth(){
        return this.width;
    }
    public double getArea(){
        return this.width*this.length;
    }
    public double getPerimeter() {
        return (this.width+this.length)*2;
    }
    public String toString(){
        String message = "";
        message = "Length: " +this.length + " Width: "+this.width+" Area: "
         + this.width * this.length + " Perimeter: "+  (this.width+this.length)*2;
         return message;
    }
}