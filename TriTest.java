package teamlab;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class TriTest {

    // this test will fail
    @Test
    public void test() {
        fail("this test will fail");
    }

    @Test
    // this test will succeed. In practice we would put
    // a real method call into the assertEquals
    public void test2() {
        Triangle t = new Triangle(6, 3, 2, 4);
        assertEquals(t.getBase(), 6);
    }

    @Test

    public void test3() {
        Triangle t = new Triangle(6, 3, 2, 4);
        assertEquals(t.getL1(), 3);
    }

    @Test

    public void test4() {
        Triangle t = new Triangle(6, 3, 2, 4);
        assertEquals(t.getL2(), 2);
    }

    @Test

    public void test5() {
        Triangle t = new Triangle(6, 3, 2, 4);
        assertEquals(t.getHeight(), 4);
    }

    @Test

    public void test6() {
        Triangle t = new Triangle(6, 3, 2, 4);
        assertEquals(t.getArea(), 12);
    }
    @Test

    public void test7() {
        Triangle t = new Triangle(6, 3, 2, 4);
        assertEquals(t.toString(), "Base: 6.0, Side 1's Length: 3.0, Side 2's Length: 2.0, Height: 4.0");
    }
}