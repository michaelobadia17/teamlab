import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
public class SquareTests { 

    // this test will fail
    @Test
    public void test() {
        fail("this test will fail");
    }

    @Test
    // this test will succeed. In practice we would put
    // a real method call into the assertEquals
    public void getLengthTest() {
        Square s1 = new Square(3,3);
        assertEquals(s1.getLength(),3);
    }
    
    @Test
    public void getWidthTest() {
        Square s2 = new Square(5.5,5.5);
        assertEquals(s2.getWidth(),5.5);
    }

    @Test 
    public void getAreaTest() { 
        Square s3 = new Square(6,6);
        assertEquals(s3.getArea(),36);
    }
    @Test
    public void getStringTest(){
        Square s4 = new Square(5,5);
        assertEquals(s4.toString(),"Length: 5.0 Width: 5.0 Area: 25.0 Perimeter: 20.0");
        
    }
}