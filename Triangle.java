package teamlab;
public class Triangle {
    private double base;
    private double length1;
    private double length2;
    private double height;

    public Triangle(double base, double length1, double length2, double height) {
        this.base = base;
        this.length1 = length1;
        this.length2 = length2;
        this.height = height;
    }
    
    public double getArea() {
        return ((this.base * this.height) / 2);
    }

    public double getPerimeter() {
        return this.base + this.length1 + this.length2;
    }

    public double getBase() {
        return this.base;
    }
    public double getL1() {
        return this.length1;
    }

    public double getL2() {
        return this.length2;
    }

    public double getHeight() {
        return this.height;
    }

    public String toString() {
        return "Base: " + this.base + ", Side 1's Length: " + this.length1 + ", Side 2's Length: " + this.length2 + ", Height: " + this.height;
    }
}
